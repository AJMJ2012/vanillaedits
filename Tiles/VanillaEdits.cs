﻿using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace VanillaEdits {
	public class VanillaTileEdits : GlobalTile {
		public override bool Drop(int x, int y, int type) {
			switch (type) {
				case TileID.BoneBlock:
					Item.NewItem(x * 16, y * 16, 16, 16, ItemID.Bone);
					return false;
			}
			return true;
		}
	}
}
