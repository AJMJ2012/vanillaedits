﻿
using System;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace VanillaEdits {
	public class VanillaEdits : Mod {
		public VanillaEdits() {
			Properties = new ModProperties {
				Autoload = true,
				AutoloadGores = true,
				AutoloadSounds = true
			};
		}

		public override void AddRecipes() {
			Recipes.VanillaEdits.AddRecipes(this);
		}
	}
}
