﻿using System;
using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace VanillaEdits.NPCs {
	public class VanillaEdits : GlobalNPC {
		public override void SetDefaults(NPC npc) {
			if (Main.expertMode && (npc.type == NPCID.SkeletronHead || npc.type == NPCID.SkeletronHand)) {
				npc.lifeMax = (int)(npc.lifeMax / 2 * 1.5);
				npc.damage = (int)(npc.damage / 2 * 1.5);
				npc.life = npc.lifeMax;
				npc.netUpdate = true;
			}
		}

		public override void AI(NPC npc) {
			Player target = Main.player[npc.target];
			int distance = (int) Math.Sqrt((npc.Center.X - target.Center.X) * (npc.Center.X - target.Center.X) + (npc.Center.Y - target.Center.Y) * (npc.Center.Y - target.Center.Y));
			if (npc.type == NPCID.ScorpionBlack || npc.type == NPCID.Scorpion || npc.type == NPCID.Bunny || npc.type == NPCID.BunnySlimed || npc.type == NPCID.BunnyXmas || npc.type == NPCID.GoldBunny || npc.type == NPCID.Firefly || npc.type == NPCID.Frog || npc.type == NPCID.GoldFrog || npc.type == NPCID.Goldfish || npc.type == NPCID.GoldfishWalker || npc.type == NPCID.Grasshopper || npc.type == NPCID.GoldGrasshopper || npc.type == NPCID.Mouse || npc.type == NPCID.GoldMouse || npc.type == NPCID.Penguin || npc.type == NPCID.PenguinBlack || npc.type == NPCID.Squirrel || npc.type == NPCID.SquirrelRed || npc.type == NPCID.SquirrelGold || npc.type == NPCID.TruffleWorm || npc.type == NPCID.TruffleWormDigger || npc.type == NPCID.Butterfly || npc.type == NPCID.GoldButterfly) {
				npc.spriteDirection = npc.direction;
				if (distance < 200) {
					npc.ai[0] = 1f;
					if (target.position.X > npc.position.X) {
						npc.direction = -1;
						if (npc.velocity.X > 0) {
							npc.velocity.X *= -1;
						}
					}
					else if (target.position.X < npc.position.X) {
						npc.direction = 1;
						if (npc.velocity.X < 0) {
							npc.velocity.X *= -1;
						}
					}
				}
			}
		}

		public override void NPCLoot(NPC npc) {
			if (npc.type == NPCID.BigPantlessSkeleton || npc.type == NPCID.SmallPantlessSkeleton || npc.type == NPCID.BigMisassembledSkeleton || npc.type == NPCID.SmallMisassembledSkeleton || npc.type == NPCID.BigHeadacheSkeleton || npc.type == NPCID.SmallHeadacheSkeleton || npc.type == NPCID.BigSkeleton || npc.type == NPCID.SmallSkeleton || npc.type == NPCID.HeavySkeleton || npc.type == NPCID.Skeleton || npc.type == NPCID.ArmoredSkeleton || npc.type == NPCID.SkeletonArcher || npc.type == NPCID.HeadacheSkeleton || npc.type == NPCID.MisassembledSkeleton || npc.type == NPCID.PantlessSkeleton || npc.type == NPCID.SkeletonTopHat || npc.type == NPCID.SkeletonAstonaut || npc.type == NPCID.SkeletonAlien || npc.type == NPCID.BoneThrowingSkeleton || npc.type == NPCID.BoneThrowingSkeleton2 || npc.type == NPCID.BoneThrowingSkeleton3 || npc.type == NPCID.BoneThrowingSkeleton4 || npc.type == NPCID.GreekSkeleton || npc.type == NPCID.BigBoned || npc.type == NPCID.ShortBones || npc.type == NPCID.BoneSerpentHead || npc.type == NPCID.DoctorBones) {
				Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Bone, Main.rand.Next(1, 4), false, 0, false, false);
			}
			if (npc.type == NPCID.SkeletonSniper || npc.type == NPCID.TacticalSkeleton || npc.type == NPCID.SkeletonCommando || npc.type == NPCID.RustyArmoredBonesAxe || npc.type == NPCID.RustyArmoredBonesFlail || npc.type == NPCID.RustyArmoredBonesSword || npc.type == NPCID.RustyArmoredBonesSwordNoArmor || npc.type == NPCID.BlueArmoredBones || npc.type == NPCID.BlueArmoredBonesMace || npc.type == NPCID.BlueArmoredBonesNoPants || npc.type == NPCID.BlueArmoredBonesSword || npc.type == NPCID.HellArmoredBones || npc.type == NPCID.HellArmoredBonesSpikeShield || npc.type == NPCID.HellArmoredBonesMace || npc.type == NPCID.HellArmoredBonesSword || npc.type == NPCID.BoneLee || npc.type == NPCID.Necromancer || npc.type == NPCID.NecromancerArmored || npc.type == NPCID.RaggedCaster || npc.type == NPCID.RaggedCasterOpenCoat || npc.type == NPCID.DiabolistRed || npc.type == NPCID.DiabolistWhite || npc.type == NPCID.GiantCursedSkull) {
				Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Bone, Main.rand.Next(2, 7), false, 0, false, false);
			}
			if (npc.type == NPCID.GreekSkeleton && Main.rand.Next(6) == 0) {
				int type = 0;
				switch (Main.rand.Next(3)) {
					case 0:
						type = ItemID.GladiatorHelmet;
						break;
					case 1:
						type = ItemID.GladiatorBreastplate;
						break;
					case 2:
						type = ItemID.GladiatorLeggings;
						break;
				}
				Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, type, 1, false, 0, false, false);
			}
			if (npc.type == NPCID.ZombieEskimo && Main.rand.Next(6) == 0)  {
				int type = 0;
				switch (Main.rand.Next(3)) {
					case 0:
						type = ItemID.EskimoCoat;
						break;
					case 1:
						type = ItemID.EskimoHood;
						break;
					case 2:
						type = ItemID.EskimoPants;
						break;
				}
				Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, type, 1, false, 0, false, false);
			}
			if (npc.type == NPCID.ArmedZombieEskimo && Main.rand.Next(6) == 0)  {
				int type = 0;
				switch (Main.rand.Next(3)) {
					case 0:
						type = ItemID.EskimoCoat;
						break;
					case 1:
						type = ItemID.EskimoHood;
						break;
					case 2:
						type = ItemID.EskimoPants;
						break;
				}
				Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, type, 1, false, 0, false, false);
			}
			if (npc.type == NPCID.ArmoredViking) {
				if (Main.rand.Next(11) == 0) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.VikingHelmet, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.Demon)  {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Blindfold, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.VoodooDemon)  {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Blindfold, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.BlackSlime) {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Blindfold, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.IceSlime)  {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.HandWarmer, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.SpikedIceSlime)  {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Toolbox, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.IceBat)  {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.HandWarmer, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.IceElemental) {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Toolbox, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.IceGolem) {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.HandWarmer, 1, false, 0, false, false);
				}
			}
			if (npc.type == NPCID.IcyMerman) {
				if ((!Main.expertMode && Main.rand.Next(201) == 0) || (Main.expertMode && Main.rand.Next(101) == 0)) {
					Item.NewItem((int)npc.position.X, (int)npc.position.Y, npc.width, npc.height, ItemID.Toolbox, 1, false, 0, false, false);
				}
			}
		}

		public virtual void SpawnNPC(NPC npc, int tileX, int tileY) {
			if (npc.type == NPCID.BlueSlime && Main.rand.Next(5) == 0 && NPC.downedSlimeKing) {
				npc.Transform(NPCID.SlimeSpiked);
			}
		}
	}
}
