﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.Localization;

namespace VanillaEdits.Items {
	public class VanillaEdits : GlobalItem {
		public override void SetDefaults(Item item) {
			switch (item.type) {
				// Accessories
				case ItemID.CobaltShield:
					item.SetNameOverride("Defender's Shield");
					break;
				case ItemID.CopperWatch:
				case ItemID.SilverWatch:
				case ItemID.GoldWatch:
				case ItemID.TinWatch:
				case ItemID.TungstenWatch:
				case ItemID.PlatinumWatch:
				case ItemID.DepthMeter:
				case ItemID.Compass:
				case ItemID.GPS:
				case ItemID.FishermansGuide:
				case ItemID.WeatherRadio:
				case ItemID.Sextant:
				case ItemID.FishFinder:
				case ItemID.MetalDetector:
				case ItemID.Stopwatch:
				case ItemID.DPSMeter:
				case ItemID.GoblinTech:
				case ItemID.TallyCounter:
				case ItemID.LifeformAnalyzer:
				case ItemID.Radar:
				case ItemID.REK:
				case ItemID.PDA:
				case ItemID.MechanicalLens:
					item.accessory = false;
					item.Prefix(-3);
					break;

				// Armor
				case ItemID.NinjaHood:
				case ItemID.GladiatorHelmet:
					item.defense = 3;
					item.rare = 1;
					break;
				case ItemID.NinjaShirt:
				case ItemID.GladiatorBreastplate:
					item.defense = 5;
					item.rare = 1;
					break;
				case ItemID.NinjaPants:
				case ItemID.GladiatorLeggings:
					item.defense = 4;
					item.rare = 1;
					break;
				case ItemID.FossilHelm:
				case ItemID.ObsidianHelm:
					item.defense = 4;
					item.rare = 1;
					break;
				case ItemID.FossilShirt:
				case ItemID.ObsidianShirt:
					item.defense = 7;
					item.rare = 1;
					break;
				case ItemID.FossilPants:
				case ItemID.ObsidianPants:
					item.defense = 5;
					item.rare = 1;
					break;
				// Weapons
				case ItemID.IronShortsword:
				case ItemID.PlatinumShortsword:
				case ItemID.TungstenShortsword:
				case ItemID.LeadShortsword:
				case ItemID.TinShortsword:
				case ItemID.CopperShortsword:
				case ItemID.SilverShortsword:
				case ItemID.GoldShortsword:
					item.useStyle = 1;
					item.useTime = (int)(item.useTime * 1.5f);
					item.useAnimation = (int)(item.useAnimation * 1.5f);
					break;
				case ItemID.Katana:
				case ItemID.BreakerBlade:
					item.useTime = (int)(item.useTime * 1.25f);
					item.useAnimation = (int)(item.useAnimation * 1.25f);
					break;
				case ItemID.BoneGlove:
				case ItemID.WandofSparking:
				case ItemID.CrimsonRod:
				case ItemID.NimbusRod:
				case ItemID.AquaScepter:
				case ItemID.Flamethrower:
				case ItemID.WaspGun:
				case ItemID.NettleBurst:
				case ItemID.FlowerPow:
				case ItemID.Seedler:
				case ItemID.LeafBlower:
				case ItemID.EldMelter: // ElfMelter
				case ItemID.PiranhaGun:
				case ItemID.Keybrand:
				case ItemID.ImpStaff:
					item.damage = (int)(item.damage * 1.5f);
					break;
				case ItemID.ReaverShark:
					item.pick = 75;
					break;
			}
		}

		public override void ModifyTooltips(Item item, List<TooltipLine> tooltips) {
			for (int i=0; i < tooltips.Count; i++) {
				switch (item.type) {
					// Accessories
					case ItemID.MagmaStone:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Inflicts fire damage on melee and thrown attacks"; }
						break;
					case ItemID.FeralClaws:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "12% increased melee and throwing speed"; }
						break;
					case ItemID.TitanGlove:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "70% increased melee and throwing knockback"; }
						break;
					case ItemID.PowerGlove:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "12% increased melee and throwing speed"; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "70% increased melee and throwing knockback"; }
						break;
					case ItemID.MechanicalGlove:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "12% increased melee and throwing damage and speed"; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "70% increased melee and throwing knockback"; }
						break;
					case ItemID.FireGauntlet:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "10% increased melee and throwing damage and speed"; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "70% increased melee and throwing knockback and inflicts fire"; }
						break;
					case ItemID.PaladinsShield:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Immunity to knockback"; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "Absorbs 25% of damage done to players on your team (Only active above 25% life)"; }
						break;
					case ItemID.FrozenTurtleShell:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Puts a shell around the owner when below 50% life that reduces damage by 25%"; }
						break;
					case ItemID.WaterWalkingBoots:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Allows the ability to walk on water and honey"; }
						break;
					case ItemID.ObsidianWaterWalkingBoots:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Allows the ability to walk on water and honey"; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "Immunity to fire blocks"; }
						break;
					case ItemID.FleshKnuckles:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "+5 defense, 5% increased melee and throwing damage and critical strike chance."; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "Enemies are more likely to target you"; }
						break;
					case ItemID.PutridScent:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "+5 defense, 5% increased magic, summon and ranged damage and critical strike chance."; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "Enemies are less likely to target you"; }
						break;

					// Armor
					case ItemID.GladiatorLeggings:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "15% increased throwing velocity"; }
						break;
					case ItemID.GladiatorBreastplate:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "15% increased throwing damage"; }
						break;
					case ItemID.GladiatorHelmet:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "10% increased throwing critical strike chance"; }
						break;
					case ItemID.ObsidianPants:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Immunity to fire blocks"; }
						break;
					case ItemID.ObsidianShirt:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Immunity to On Fire! debuff"; }
						break;
					case ItemID.ObsidianHelm:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Increases movement speed by 10%"; }
						break;
					case ItemID.HallowedMask:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "10% increased melee damage and critical strike chance"; }
						if (tooltips[i].Name == "Tooltip1") { tooltips[i].text = "10% increased melee speed"; }
						break;
					case ItemID.ShroomiteHeadgear:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "15% increased arrow and throwing damage"; }
						break;
					case ItemID.ShroomiteBreastplate:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "13% increased ranged and throwing damage and critical strike chance"; }
						break;
					case ItemID.ShroomiteLeggings:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "7% increased ranged and throwing critical strike chance"; }
						break;
					case ItemID.VikingHelmet:
						if (tooltips[i].Name == "Tooltip0") { tooltips[i].text = "Attackers take 33% damage"; }
						break;
				}
			}
		}

		public override void UpdateAccessory(Item item, Player player, bool hide) {
			switch (item.type) {
				case ItemID.RoyalGel:
					if (!hide) { player.drippingSlime = true; }
					else { player.drippingSlime = false; }
					break;
			}
		}

		public override void UpdateEquip(Item item, Player player) {
			switch (item.type) {
				// Accessories
				case ItemID.FleshKnuckles:
					player.statDefense -= 2;
					player.meleeDamage += 0.05f;
					player.thrownDamage += 0.05f;
					player.meleeCrit += 5;
					player.thrownCrit += 5;
					break;
				case ItemID.PutridScent:
					player.statDefense += 5;
					player.meleeDamage /= 0.5f;
					player.thrownDamage /= 0.5f;
					player.meleeCrit -= 5;
					player.thrownCrit -= 5;
					break;
				case ItemID.FeralClaws:
					player.thrownVelocity += 0.12f;
					break;
				case ItemID.TitanGlove:
					break;
				case ItemID.PowerGlove:
					player.thrownVelocity += 0.12f;
					break;
				case ItemID.MechanicalGlove:
					player.thrownVelocity += 0.12f;
					player.thrownDamage += 0.12f;
					break;
				case ItemID.FireGauntlet:
					player.thrownVelocity += 0.10f;
					player.thrownDamage += 0.10f;
					break;

				// Armor
				case ItemID.VikingHelmet:
					player.thorns += 0.333333343f;
					break;
				case ItemID.GladiatorHelmet:
					player.thrownVelocity += 0.15f;
					break;
				case ItemID.GladiatorBreastplate:
					player.thrownDamage += 0.15f;
					break;
				case ItemID.GladiatorLeggings:
					player.thrownCrit += 10;
					break;
				case ItemID.ObsidianPants:
					player.fireWalk = true;
					break;
				case ItemID.CobaltMask:
					player.thrownDamage += 0.1f;
					player.thrownCrit += 6;
					break;
				case ItemID.CobaltBreastplate:
					player.thrownCrit += 3;
					break;
				case ItemID.MythrilHat:
					player.thrownDamage += 0.12f;
					player.thrownCrit += 7;
					break;
				case ItemID.MythrilChainmail:
					player.thrownDamage += 0.05f;
					player.minionDamage += 0.05f;
					break;
				case ItemID.MythrilGreaves:
					player.thrownCrit += 3;
					break;
				case ItemID.AdamantiteMask:
					player.thrownDamage += 0.14f;
					player.thrownCrit += 8;
					break;
				case ItemID.AdamantiteBreastplate:
					player.thrownDamage += 0.06f;
					player.minionDamage += 0.06f;
					break;
				case ItemID.AdamantiteLeggings:
					player.thrownCrit += 4;
					break;
				case ItemID.PalladiumHelmet:
					player.thrownDamage += 0.09f;
					player.thrownCrit += 9;
					break;
				case ItemID.PalladiumBreastplate:
					player.thrownDamage += 0.03f;
					player.minionDamage += 0.03f;
					player.thrownCrit += 2;
					break;
				case ItemID.PalladiumLeggings:
					player.thrownDamage += 0.02f;
					player.minionDamage += 0.02f;
					player.thrownCrit++;
					break;
				case ItemID.OrichalcumHelmet:
					player.thrownCrit += 15;
					break;
				case ItemID.OrichalcumBreastplate:
					player.thrownCrit += 6;
					break;
				case ItemID.TitaniumHelmet:
					player.thrownDamage += 0.16f;
					player.thrownCrit += 7;
					break;
				case ItemID.TitaniumBreastplate:
					player.thrownDamage += 0.04f;
					player.minionDamage += 0.04f;
					player.thrownCrit += 3;
					break;
				case ItemID.TitaniumLeggings:
					player.thrownDamage += 0.03f;
					player.minionDamage += 0.03f;
					player.thrownCrit += 3;
					break;
				case ItemID.HallowedHelmet:
					player.thrownDamage += 0.15f;
					player.thrownCrit += 8;
					break;
				case ItemID.HallowedPlateMail:
					player.thrownCrit += 7;
					break;
				case ItemID.HallowedGreaves:
					player.thrownDamage += 0.07f;
					player.minionDamage += 0.07f;
					break;
				case ItemID.NecroHelmet:
				case ItemID.NecroBreastplate:
				case ItemID.NecroGreaves:
				case ItemID.AncientNecroHelmet:
					player.thrownDamage += 0.05f;
					break;
				case ItemID.CrimsonHelmet:
				case ItemID.CrimsonScalemail:
				case ItemID.CrimsonGreaves:
					player.minionDamage += 0.02f;
					break;
				case ItemID.FrostHelmet:
					player.thrownDamage += 0.16f;
					break;
				case ItemID.FrostBreastplate:
					player.thrownCrit += 11;
					break;
				case ItemID.ChlorophyteHelmet:
					player.thrownDamage += 0.16f;
					break;
				case ItemID.ChlorophytePlateMail:
					player.thrownDamage += 0.05f;
					player.minionDamage += 0.05f;
					player.thrownCrit += 7;
					break;
				case ItemID.ChlorophyteGreaves:
					player.thrownCrit += 8;
					break;
				case ItemID.ShroomiteHeadgear:
					player.thrownDamage += 0.15f;
					break;
				case ItemID.ShroomiteBreastplate:
					player.thrownDamage += 0.13f;
					player.thrownCrit += 13;
					player.thrownCost33 = true;
					break;
				case ItemID.ShroomiteLeggings:
					player.thrownCrit += 7;
					break;
				case ItemID.VortexHelmet:
					player.thrownCrit += 7;
					player.thrownDamage += 0.16f;
					break;
				case ItemID.VortexBreastplate:
					player.thrownCrit += 12;
					player.thrownDamage += 0.12f;
					break;
				case ItemID.VortexLeggings:
					player.thrownCrit += 8;
					player.thrownDamage += 0.08f;
					break;
				case ItemID.Gi:
					player.minionDamage += 0.05f;
					break;
			}
		}

		public override string IsArmorSet(Item head, Item body, Item legs) {
			if (head.type == ItemID.PinkEskimoHood && body.type == ItemID.PinkEskimoCoat && legs.type == ItemID.PinkEskimoPants) {
				return "Eskimo";
			}
			if (head.type == ItemID.EskimoHood && body.type == ItemID.EskimoCoat && legs.type == ItemID.EskimoPants) {
				return "Eskimo";
			}
			if (head.type == ItemID.ObsidianHelm && body.type == ItemID.ObsidianShirt && legs.type == ItemID.ObsidianPants) {
				return "Obsidian";
			}
			if (head.type == ItemID.GladiatorHelmet && body.type == ItemID.GladiatorBreastplate && legs.type == ItemID.GladiatorLeggings) {
				return "Gladiator";
			}
			if (head.type == ItemID.VortexHelmet && body.type == ItemID.VortexBreastplate && legs.type == ItemID.VortexLeggings) {
				return "Vortex";
			}
			if ((head.type == ItemID.ShroomiteHeadgear || head.type == ItemID.ShroomiteMask || head.type == ItemID.ShroomiteHelmet) && body.type == ItemID.ShroomiteBreastplate && legs.type == ItemID.ShroomiteLeggings) {
				return "Shroomite";
			}
			return "";
		}

		public override void UpdateArmorSet(Player player, string set) {
			switch (set) {
				case "Eskimo":
					player.resistCold = true;
					player.setBonus = "Reduces damage from cold sources";
					break;
				case "Gladiator":
					player.thrownCost33 = true;
					player.setBonus = Language.GetTextValue("ArmorSetBonus.Ninja");
					break;
				case "Obsidian":
					player.lavaRose = true;
					player.lavaMax += 180;
					player.setBonus = "Immunity to On Fire! debuff, Provides 3 seconds of immunity to lava, Reduced damage from touching lava";
					break;
				case "Vortex":
					player.thrownDamage += (1f - player.stealth) * 0.8f;
					player.thrownCrit += (int)(((1f - player.stealth) * 0.2f) * 100f);
					break;
				case "Shroomite":
					player.thrownDamage += (1f - player.stealth) * 0.6f;
					player.thrownCrit += (int)(((1f - player.stealth) * 0.1f) * 100f);
					break;
			}
		}
	}
}