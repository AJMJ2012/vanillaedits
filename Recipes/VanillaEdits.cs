using Terraria;
using Terraria.ModLoader;
using Terraria.ID;

namespace VanillaEdits.Recipes {
	public static class VanillaEdits {
		public static void AddRecipes(Mod mod) {
			RecipeGroup.RegisterGroup("VESilverBar", new RecipeGroup(
				() => Lang.misc[37].Value + " " + Lang.GetItemNameValue(ItemID.SilverBar),
				new int[] {
					ItemID.SilverBar,
					ItemID.TungstenBar
				}
			));

			RecipeGroup.RegisterGroup("VEHermesBoots", new RecipeGroup(
				() => Lang.misc[37].Value + " " + Lang.GetItemNameValue(ItemID.HermesBoots),
				new int[] {
					ItemID.HermesBoots,
					ItemID.FlurryBoots,
					ItemID.SailfishBoots
				}
			));

			ModRecipe modRecipe = new ModRecipe(mod);
			/* Additional Bone Welder Recipes */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Bone, 10);
			modRecipe.AddTile(TileID.BoneWelder);
			modRecipe.SetResult(ItemID.BonePickaxe, 1);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Bone, 10);
			modRecipe.AddTile(TileID.BoneWelder);
			modRecipe.SetResult(ItemID.BoneSword, 1);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Bone, 1);
			modRecipe.AddTile(TileID.BoneWelder);
			modRecipe.SetResult(ItemID.BoneArrow, 3);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Bone, 1);
			modRecipe.AddIngredient(ItemID.Torch, 1);
			modRecipe.AddTile(TileID.BoneWelder);
			modRecipe.SetResult(ItemID.BoneTorch, 1);
			modRecipe.AddRecipe();

			/* Ammo */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.MusketBall, 70);
			modRecipe.AddIngredient(ItemID.TungstenBar, 1);
			modRecipe.AddTile(TileID.Anvils);
			modRecipe.SetResult(ItemID.SilverBullet, 70);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddRecipeGroup("IronBar", 1);
			modRecipe.AddTile(TileID.Anvils);
			modRecipe.SetResult(ItemID.MusketBall, 20);
			modRecipe.AddRecipe();

			/* Basic Throwing Weapons */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddRecipeGroup("IronBar", 1);
			modRecipe.AddTile(TileID.Anvils);
			modRecipe.SetResult(ItemID.Shuriken, 30);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddRecipeGroup("VESilverBar", 1);
			modRecipe.AddTile(TileID.Anvils);
			modRecipe.SetResult(ItemID.ThrowingKnife, 30);
			modRecipe.AddRecipe();

			/* Chain Lantern */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Chain, 1);
			modRecipe.AddIngredient(ItemID.Glass, 4);
			modRecipe.AddIngredient(ItemID.Torch, 1);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.ChainLantern, 1);
			modRecipe.AddRecipe();

			/* Rare Accessories Craftable */
			// > Obsidian Rose
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.JungleRose, 1);
			modRecipe.AddIngredient(ItemID.Obsidian, 2);
			modRecipe.AddTile(TileID.TinkerersWorkbench);
			modRecipe.SetResult(ItemID.ObsidianRose, 1);
			modRecipe.AddRecipe();

			// > Jellyfish Necklace (Specifically requires Pink Jellyfish)
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.PinkJellyfish, 3);
			modRecipe.AddIngredient(ItemID.Chain, 2);
			modRecipe.AddIngredient(ItemID.BouncyGlowstick, 2);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.JellyfishNecklace, 1);
			modRecipe.AddRecipe();

			// > Frozen Turtle Shell
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.TurtleShell, 1);
			modRecipe.AddIngredient(ItemID.FrostCore, 1);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.FrozenTurtleShell, 1);
			modRecipe.AddRecipe();

			// > Band of Starpower (You lose the chian)
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.PanicNecklace, 1);
			modRecipe.AddIngredient(ItemID.ManaCrystal, 4);
			modRecipe.AddTile(TileID.TinkerersWorkbench);
			modRecipe.SetResult(ItemID.BandofStarpower, 1);
			modRecipe.AddRecipe();

			// > Panic Necklace
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.BandofStarpower, 1);
			modRecipe.AddIngredient(ItemID.LifeCrystal, 2);
			modRecipe.AddIngredient(ItemID.Chain, 2);
			modRecipe.AddTile(TileID.TinkerersWorkbench);
			modRecipe.SetResult(ItemID.PanicNecklace, 1);
			modRecipe.AddRecipe();

			// > Diving Helmet
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.CopperBar, 15);
			modRecipe.AddIngredient(ItemID.TinBar, 15);
			modRecipe.AddIngredient(ItemID.Glass, 10);
			modRecipe.AddTile(TileID.Anvils);
			modRecipe.SetResult(ItemID.DivingHelmet, 1);
			modRecipe.AddRecipe();

			// > Water Walking Boots
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.SailfishBoots, 1);
			modRecipe.AddIngredient(ItemID.SoulofFright, 10);
			modRecipe.AddIngredient(ItemID.SoulofFlight, 10);
			modRecipe.AddTile(TileID.CrystalBall);
			modRecipe.SetResult(ItemID.WaterWalkingBoots, 1);
			modRecipe.AddRecipe();

			// > Lava Charm
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.LavaBucket, 1);
			modRecipe.AddIngredient(ItemID.ObsidianRose, 1);
			modRecipe.AddIngredient(ItemID.ObsidianSkinPotion, 1);
			modRecipe.AddIngredient(ItemID.LivingFireBlock, 10);
			modRecipe.AddIngredient(ItemID.SoulofMight, 10);
			modRecipe.AddTile(TileID.CrystalBall);
			modRecipe.SetResult(ItemID.LavaCharm, 1);
			modRecipe.AddRecipe();

			// > Cloud in a Bottle
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Bottle, 1);
			modRecipe.AddIngredient(ItemID.Cloud, 10);
			modRecipe.AddIngredient(ItemID.SoulofFlight, 5);
			modRecipe.AddTile(TileID.CrystalBall);
			modRecipe.SetResult(ItemID.CloudinaBottle, 1);
			modRecipe.AddRecipe();

			// > Blizzard in a Bottle
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Bottle, 1);
			modRecipe.AddIngredient(ItemID.FrostCore , 1);
			modRecipe.AddIngredient(ItemID.SoulofFlight, 10);
			modRecipe.AddTile(TileID.CrystalBall);
			modRecipe.SetResult(ItemID.BlizzardinaBottle, 1);
			modRecipe.AddRecipe();

			// > Sandstorm in a Bottle
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Bottle, 1);
			modRecipe.AddIngredient(3783 , 1);
			modRecipe.AddIngredient(ItemID.SoulofFlight, 20);
			modRecipe.AddTile(TileID.CrystalBall);
			modRecipe.SetResult(ItemID.SandstorminaBottle, 1);
			modRecipe.AddRecipe();

			// > Ice Skates
			modRecipe = new ModRecipe(mod);
			modRecipe.AddRecipeGroup("VEHermesBoots", 1);
			modRecipe.AddIngredient(ItemID.Silk, 10);
			modRecipe.AddRecipeGroup("IronBar", 5);
			modRecipe.AddTile(TileID.TinkerersWorkbench);
			modRecipe.SetResult(ItemID.IceSkates, 1);
			modRecipe.AddRecipe();

			/* Slime Staff */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Gel, 250);
			modRecipe.AddIngredient(ItemID.Wood, 10);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.SlimeStaff, 1);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.PinkGel, 50);
			modRecipe.AddIngredient(ItemID.Wood, 10);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.SlimeStaff, 1);
			modRecipe.AddRecipe();

			/* Chain Knife */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddRecipeGroup("VESilverBar", 15);
			modRecipe.AddIngredient(ItemID.Chain, 2);
			modRecipe.AddTile(TileID.Anvils);
			modRecipe.SetResult(ItemID.ChainKnife, 1);
			modRecipe.AddRecipe();

			/* Restore Wooden Beam */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Gel, 1);
			modRecipe.AddIngredient(ItemID.WoodenBeam, 2);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.Wood, 1);
			modRecipe.AddRecipe();

			/* Rain Cloud */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.Cloud, 1);
			modRecipe.AddTile(TileID.Sinks);
			modRecipe.SetResult(ItemID.RainCloud, 1);
			modRecipe.AddRecipe();

			/* Traps */
			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.GrayBrick, 2);
			modRecipe.AddIngredient(ItemID.Wire, 10);
			modRecipe.AddIngredient(ItemID.PoisonDart, 100);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.DartTrap, 1);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.LihzahrdBrick, 3);
			modRecipe.AddIngredient(ItemID.Wire, 20);
			modRecipe.AddIngredient(ItemID.WrathPotion, 1);
			modRecipe.AddIngredient(ItemID.PoisonDart, 100);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.SuperDartTrap, 1);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.LihzahrdBrick, 3);
			modRecipe.AddIngredient(ItemID.Wire, 20);
			modRecipe.AddIngredient(ItemID.Chain, 15);
			modRecipe.AddIngredient(ItemID.WrathPotion, 1);
			modRecipe.AddIngredient(ItemID.Spike, 5);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.SpearTrap, 1);
			modRecipe.AddRecipe();

			modRecipe = new ModRecipe(mod);
			modRecipe.AddIngredient(ItemID.LihzahrdBrick, 3);
			modRecipe.AddIngredient(ItemID.Wire, 20);
			modRecipe.AddIngredient(ItemID.WrathPotion, 1);
			modRecipe.AddIngredient(ItemID.SpikyBall, 100);
			modRecipe.AddTile(TileID.WorkBenches);
			modRecipe.SetResult(ItemID.SpikyBallTrap, 1);
			modRecipe.AddRecipe();
		}
	}
}