﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;

namespace TerrariaMod.Projectiles {
	public class VanillaEdits : GlobalProjectile {



		public override void AI(Projectile projectile) {
			if (projectile.aiStyle == 19) {
				float start = 5f;
				float mult = 1f;
				switch (projectile.type) {
					case ProjectileID.DarkLance:
					case ProjectileID.TheRottedFork:
					case ProjectileID.NorthPoleWeapon:
						mult = 1.25f;
						break;
					case ProjectileID.CobaltNaginata:
					case ProjectileID.PalladiumPike:
						mult = 1.5f;
						break;
					case ProjectileID.MythrilHalberd:
					case ProjectileID.OrichalcumHalberd:
						mult = 1.75f;
						break;
					case ProjectileID.AdamantiteGlaive:
					case ProjectileID.Gungnir:
					case ProjectileID.TitaniumTrident:
					case ProjectileID.ChlorophytePartisan:
						mult = 2f;
						break;
				} {
					if ((double) projectile.localAI[1] == 0.0) {
						projectile.localAI[1] = start * mult;
						projectile.damage = (int)(projectile.damage * 1.5f);
					}
					if (Main.player[projectile.owner].itemAnimation < Main.player[projectile.owner].itemAnimationMax / 1.333333333333333)
						projectile.localAI[1] -= 0.375f * mult;
					else
						projectile.localAI[1] += 2f * mult;
					projectile.ai[0] = projectile.localAI[1];
				}
			}
			if (projectile.thrown && Main.player[projectile.owner].magmaStone && !projectile.noEnchantments && Main.rand.Next(3) != 0) {
				int index = Dust.NewDust(new Vector2(projectile.position.X - 4f, projectile.position.Y - 4f), projectile.width + 8, projectile.height + 8, 6, projectile.velocity.X * 0.2f, projectile.velocity.Y * 0.2f, 100, new Color(), 2f);
				if (Main.rand.Next(2) == 0)
					Main.dust[index].scale = 1.5f;
				Main.dust[index].noGravity = true;
				Main.dust[index].velocity.X *= 2f;
				Main.dust[index].velocity.Y *= 2f;
			}
		}

		public override void OnHitNPC(Projectile projectile, NPC target, int damage, float knockback, bool crit) {
			if (projectile.thrown && Main.player[projectile.owner].magmaStone && !projectile.noEnchantments) {
				if (Main.rand.Next(7) == 0)
					target.AddBuff(24, 360, false);
				else if (Main.rand.Next(3) == 0)
					target.AddBuff(24, 120, false);
				else
					target.AddBuff(24, 60, false);
			}
		}
	}
}